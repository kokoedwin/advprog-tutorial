package tutorial.javari;


import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tutorial.hello.Greeting;
import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

@RestController
public class JavariController {

    private AnimalData animalData = new AnimalData();

    private final Greeting error = new Greeting(404, "Animal not found!");
    private ObjectWriter ow = new ObjectMapper().writerWithDefaultPrettyPrinter();
    private Animal animal;

    @RequestMapping(method = POST, value = "/javari")
    public Animal addAnimal(
            @RequestParam(value = "name", defaultValue = "none") String name,
            @RequestParam(value = "type", defaultValue = "none") String type,
            @RequestParam(value = "gender", defaultValue = "male") String gender,
            @RequestParam(value = "length", defaultValue = "0") String length,
            @RequestParam(value = "weight", defaultValue = "0") String weight,
            @RequestParam(value = "condition", defaultValue = "healthy") String condition
    ) {
        return animalData.addAnimal(
                type,
                name,
                Gender.parseGender(gender),
                Double.parseDouble(length),
                Double.parseDouble(weight),
                Condition.parseCondition(condition)
        );
    }

    @RequestMapping(method = GET, value = "/javari")
    public String showAnimals() throws JsonProcessingException {
        if (animalData.getSize() <= 0) {
            return ow.writeValueAsString(error);
        }
        return ow.writeValueAsString(animalData.getAnimals());
    }

    @RequestMapping(method = GET, value = "/javari/{id}")
    public String showAnimals(@PathVariable String id) throws JsonProcessingException {
        animal = animalData.findAnimal(Integer.parseInt(id));
        if (animal == null) {
            return ow.writeValueAsString(error);
        }
        return ow.writeValueAsString(animal);
    }

    @RequestMapping(method = DELETE, value = "/javari/{id}")
    public String deleteAnimal(@PathVariable String id) throws JsonProcessingException {
        animal = animalData.deleteAnimalWithId(Integer.parseInt(id));
        if (animal == null) {
            return ow.writeValueAsString(error);
        }
        return ow.writeValueAsString(animal);
    }
}
