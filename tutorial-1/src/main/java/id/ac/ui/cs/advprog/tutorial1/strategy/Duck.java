package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    /// TODO Complete me!
    public void swim(){
      System.out.println("All ducks float, even decoys");
    }

    public void setFlyBehavior(FlyBehavior flying){
      this.flyBehavior = flying;
    }

    public void setQuackBehavior(QuackBehavior quacking){
      this.quackBehavior = quacking;
    }

    public abstract void display();

}
