package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class DirtyClams implements Clams {

    public String toString() {
        return "Dirty Clams from market";
    }
}
