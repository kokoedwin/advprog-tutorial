import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

      public String statement() {
      double totalAmount = 0;
      int frequentRenterPoints = 0;

      Iterator<Rental> iterator = rentals.iterator();
      String result = "Rental Record for " + getName() + "\n";
      while (iterator.hasNext()) {
          Rental each = iterator.next();
          double thisAmount = 0;

          thisAmount = each.getCharge();

          frequentRenterPoints += each.getFrequentRenterPoints();
          // Show figures for this rental
          result += "\t" + each.getMovie().getTitle() + "\t"
                  + String.valueOf(each.getCharge()) + "\n";
          totalAmount += each.getCharge();
      }

      // Add footer lines
      result += "Amount owed is " + String.valueOf(getTotalCharge())
                + "\n";
      result += "You earned " + String.valueOf(getTotalFrequentRenterPoints())
                + " frequent renter points";
      return result;
  }

  private double amountFor(Rental arental) {
      return arental.getCharge();
  }

  private double getTotalCharge() {
      double result = 0;
      for (Rental each : rentals) {
          result +=  each.getCharge();
      }
      return result;
  }

  private int getTotalFrequentRenterPoints() {
      int result = 0;
      for (Rental each : rentals) {
          result += each.getFrequentRenterPoints();
      }
      return result;
  }

  public String htmlStatement() {
      String result = "<h1>Rentals for <EM>" + getName() + "</EM></H1><P>\n";
      for (Rental each : rentals) {
          result += each.getMovie().getTitle() + ": "
                  + String.valueOf(each.getCharge()) + "<BR>\n";
      }
      result += "<p>You owe <em>" + String.valueOf(getTotalCharge()) + "</em><p>\n";
      result += "On this rental you earned <em>"
              + String.valueOf(getTotalFrequentRenterPoints())
              + "</em> frequent renter points<p>";
      return result;
  }

}
