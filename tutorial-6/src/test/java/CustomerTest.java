import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CustomerTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable
    Customer customer = new Customer("Alice");

    @Test
    public void getName() {
        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        Rental rent = new Rental(movie, 3);

        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    // TODO Implement me!
    @Test
    public void statementWithMultipleMovies() {
        Movie movie1 = new Movie("a?", Movie.REGULAR);
        Rental rent1 = new Rental(movie1, 1);
        Movie movie2 = new Movie("b?", Movie.REGULAR);
        Rental rent2 = new Rental(movie2, 2);
        Movie movie3 = new Movie("c?", Movie.REGULAR);
        Rental rent3 = new Rental(movie3, 3);
        customer.addRental(rent1);
        customer.addRental(rent2);
        customer.addRental(rent3);
        String result = customer.htmlStatement();
        String[] lines = result.split("\n");
        assertEquals(6, lines.length);
        assertTrue(result.contains("<p>You owe <em>7.5</em><p>"));
        assertTrue(result.contains("On this rental you earned <em>3<"
            + "/em> frequent renter points<p>"));
    }
}
